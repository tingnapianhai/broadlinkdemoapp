package cn.com.broadlink.blappsdkdemo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import cn.com.broadlink.blappsdkdemo.BLApplcation;
import cn.com.broadlink.blappsdkdemo.R;
import cn.com.broadlink.sdk.BLLet;
import cn.com.broadlink.sdk.data.controller.BLDNADevice;
import cn.com.broadlink.sdk.interfaces.controller.BLDeviceScanListener;

public class DevListActivity extends AppCompatActivity {

    public final static String INTENT_DEV_ID = "INTENT_DEV_ID";

    private ListView mDevListView;
    private DeviceAdapter mDeviceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dev_list_layout);
        setTitle("Device List");

        initView();
        initAdapter();
    }

    private void initView() {
        mDevListView = (ListView) findViewById(R.id.dev_list);
        mDevListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra(INTENT_DEV_ID, BLApplcation.mDevList.get(position));
                intent.setClass(DevListActivity.this, DevMoreActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.btn_configure_device).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configureDevice();
            }
        });
    }

    private void configureDevice() {
        Intent intent = new Intent();
        intent.setClass(DevListActivity.this, DevConfigActivity.class);
        startActivity(intent);
    }

    private void initAdapter() {
        mDeviceAdapter = new DeviceAdapter(DevListActivity.this, BLApplcation.mDevList);
        mDevListView.setAdapter(mDeviceAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        devProbleListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BLLet.Controller.stopProbe();
    }

    private void devProbleListener() {
        BLLet.Controller.startProbe();
        BLLet.Controller.setOnDeviceScanListener(new BLDeviceScanListener() {

            @Override
            public boolean shouldAdd(BLDNADevice bldnaDevice) {
                return true;
            }

            @Override
            public void onDeviceUpdate(BLDNADevice dnaDevice, boolean isNewDev) {
                Log.i("dev info ", dnaDevice.getName());
                if (isNewDev) {
                    BLApplcation.mDevList.add(dnaDevice);
                } else {
                    //TODO 更新设备信息
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mDeviceAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    //设备列表适配器
    private class DeviceAdapter extends ArrayAdapter<BLDNADevice> {
        public DeviceAdapter(Context context, List<BLDNADevice> objects) {
            super(context, 0, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = getLayoutInflater().inflate(R.layout.adapter_device, null);
                viewHolder.name = (TextView) convertView.findViewById(R.id.tv_name);
                viewHolder.mac = (TextView) convertView.findViewById(R.id.tv_mac);
                viewHolder.ip = (TextView) convertView.findViewById(R.id.tv_ip);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            BLDNADevice device = getItem(position);

            viewHolder.name.setText(device.getName());
            viewHolder.mac.setText(device.getMac());
            viewHolder.ip.setText("" + BLLet.Controller.queryDeviceIp(device.getDid()));

            return convertView;
        }

        private class ViewHolder {
            TextView name;
            TextView mac;
            TextView ip;
        }
    }

}
