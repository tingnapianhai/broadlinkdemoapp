package cn.com.broadlink.blappsdkdemo;

import android.app.Application;

import java.util.ArrayList;

import cn.com.broadlink.blappsdkdemo.common.BLUserInfoUnits;
import cn.com.broadlink.sdk.data.controller.BLDNADevice;

public class BLApplcation extends Application {

    public static ArrayList<BLDNADevice> mDevList = new ArrayList<BLDNADevice>();

    public static BLUserInfoUnits mBLUserInfoUnits;

    @Override
    public void onCreate() {
        super.onCreate();

        mBLUserInfoUnits = new BLUserInfoUnits(this);
    }

}
