package cn.com.broadlink.blappsdkdemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import cn.com.broadlink.blappsdkdemo.BLApplcation;
import cn.com.broadlink.blappsdkdemo.R;

public class MainActivity extends Activity {

    private ListView mOperateListView;
    private List<String> mOperateList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mOperateList = new ArrayList<String>();
        mOperateList.add("Device List");
//        mOperateList.add("家庭管理");
//        mOperateList.add("红码测试");

        findView();
        setListener();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mOperateList);
        mOperateListView.setAdapter(adapter);
    }

    private void findView() {
        mOperateListView = (ListView) findViewById(R.id.operate_list);
    }

    private void setListener() {
        mOperateListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();

                switch (position) {
                    case 0:
                        intent.setClass(MainActivity.this, DevListActivity.class);
                        break;
                    case 1:
                        intent.setClass(MainActivity.this, FamilyListActivity.class);
                        break;
                    case 2:
                        intent.setClass(MainActivity.this, IRCodeTestActivity.class);
                        break;
                    default:
                        return;
                }

                startActivity(intent);
            }
        });
    }

    //退出登录
    public void logOut(View view) {
        BLApplcation.mBLUserInfoUnits.loginOut();
        Intent intent = new Intent();
        intent.setClass(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        MainActivity.this.finish();
    }

}
