package cn.com.broadlink.blappsdkdemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import cn.com.broadlink.blappsdkdemo.BLApplcation;
import cn.com.broadlink.blappsdkdemo.R;
import cn.com.broadlink.sdk.BLLet;
import cn.com.broadlink.sdk.param.family.BLFamilyIdInfo;
import cn.com.broadlink.sdk.result.account.BLLoginResult;
import cn.com.broadlink.sdk.result.family.BLFamilyIdListGetResult;

public class FamilyListActivity extends Activity {

    private ListView mFamilyIdListView;
    private List<String> mFamilyIdList;
    private ArrayAdapter<String> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_list);

        findView();
        setListener();

        mFamilyIdList = new ArrayList<>();
        mAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mFamilyIdList);
        mFamilyIdListView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFamilyListFormCloud();
    }

    private void findView() {
        mFamilyIdListView = (ListView) findViewById(R.id.family_id_list);
    }

    private void setListener(){
        mFamilyIdListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("INTENT_FAMILY_ID", mFamilyIdList.get(position));
                intent.setClass(FamilyListActivity.this, FamilyDetailActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getFamilyListFormCloud() {
        new FamilyIdListTask().execute();
    }

    private class FamilyIdListTask extends AsyncTask<String, Void, BLFamilyIdListGetResult> {
        @Override
        protected BLFamilyIdListGetResult doInBackground(String... strings) {
            return BLLet.Family.queryLoginUserFamilyIdList();
        }

        @Override
        protected void onPostExecute(BLFamilyIdListGetResult blFamilyIdListGetResult) {
            super.onPostExecute(blFamilyIdListGetResult);
            if (blFamilyIdListGetResult != null && blFamilyIdListGetResult.succeed()) {
                mFamilyIdList.clear();
                for (BLFamilyIdInfo info:
                        blFamilyIdListGetResult.getIdInfoList()) {
                    mFamilyIdList.add(info.getFamilyId());
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
