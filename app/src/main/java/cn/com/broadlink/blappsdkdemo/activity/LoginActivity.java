package cn.com.broadlink.blappsdkdemo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import cn.com.broadlink.blappsdkdemo.BLApplcation;
import cn.com.broadlink.blappsdkdemo.R;
import cn.com.broadlink.sdk.BLLet;
import cn.com.broadlink.sdk.constants.account.BLAccountErrCode;
import cn.com.broadlink.sdk.result.account.BLLoginResult;

public class LoginActivity extends Activity{

    private final static String THIRD_AUTH_ID = "20";

    private Button mLoginWithoutNameBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        initView();
    }

    private void initView(){
        mLoginWithoutNameBtn = (Button) findViewById(R.id.btn_login_without_name);
        mLoginWithoutNameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LoginWithoutNameTask().execute();
            }
        });
    }

    private class LoginWithoutNameTask extends AsyncTask<String, Void, BLLoginResult>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected BLLoginResult doInBackground(String... params) {
            return BLLet.Account.thirdAuth(THIRD_AUTH_ID);
        }

        @Override
        protected void onPostExecute(BLLoginResult loginResult) {
            super.onPostExecute(loginResult);
            progressDialog.dismiss();

            if(loginResult != null && loginResult.getError() == BLAccountErrCode.SUCCESS){
                //保存登录信息
                BLApplcation.mBLUserInfoUnits.login(loginResult.getUserid(),
                        loginResult.getLoginsession(), loginResult.getNickname(),
                        loginResult.getIconpath(), loginResult.getLoginip(),
                        loginResult.getLogintime(), loginResult.getSex(), null);

                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
            }
        }

    }
}
