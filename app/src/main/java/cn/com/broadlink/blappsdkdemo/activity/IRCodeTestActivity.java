package cn.com.broadlink.blappsdkdemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.com.broadlink.blappsdkdemo.R;
import cn.com.broadlink.blappsdkdemo.common.BLLog;
import cn.com.broadlink.blappsdkdemo.common.BLStorageUtils;
import cn.com.broadlink.blirdaconlib.BLIrdaConLib;
import cn.com.broadlink.blirdaconlib.BLIrdaConProduct;
import cn.com.broadlink.sdk.BLLet;
import cn.com.broadlink.sdk.data.controller.BLRMCloudAcConstants;
import cn.com.broadlink.sdk.param.controller.BLQueryIRCodeParams;
import cn.com.broadlink.sdk.result.controller.BLBaseBodyResult;
import cn.com.broadlink.sdk.result.controller.BLDownloadScriptResult;
import cn.com.broadlink.sdk.result.controller.BLIRCodeDataResult;
import cn.com.broadlink.sdk.result.controller.BLIRCodeInfoResult;

public class IRCodeTestActivity extends Activity {

    private ListView mIRCodeTestList;
    private List<String> mOperateList;
    private final String LOG_TAG = "IRCODETEST";

    private String mDownloadUrl;
    private String mScriptRandkey;
    private String mScriptName;
    private String mSavePath;

    //Media AC
//    private String ircodeHexString = "2600ca008d950c3b0f1410380e3a0d160e160d3b0d150e150e3910150d160d3a0f36101411380d150f3a0e390d3910370f150f38103a0d3a0e1211140f1411121038101310150f3710380e390e150f160d160e1410140f131113101310380e3b0f351137123611ad8e9210370f1511370e390f140f1410380f1311130f39101211130f390f380f150f390f1310380f3810380f380f141038103710380f1411121014101310380f14101310380f3810381013101311121014101211131014101310370f3910361138103710000d05";

    //NewMax STB
//    private String ircodeHexString = "26005800000127941215101413111114111510141213121213380f3a1238133712381237121311391238121313120f3a153611131239111412131237133711131314113714111337120005440001264b10000c560001264d10000d05";

    //Bein STB
//    private String ircodeHexString = "260042001c1e3b1d1d3a3a3b391e1d1e1d3a1c1e3a1e1d000baa1c1f391e1e393b3a391f1c1e1b3c1c1e3a1e1e000ba91c1e3b1d1d3a3b3a391e1d1e1c3b1e1c3a1e1e000d05000000000000";

    //OSN STB
    private String ircodeHexString = "26007c00551d0e100d100d1d0e1d2b1d0e0f0e0f0d100d100d100d0f0e0f1c1e1c1d1c100d1e1b1e1c1e0d101b100e0f0d1e0d100d0f0e0f1c100d1e0d0f0e0008d5561c0f0f0f0e0d1e0f1b2d1c0d100f0d100d0f0e0f0e0f0e0d0f1e1c1c1d1e0e101b1e1b1c1e0f0e1c0f100d0e1d0e0f0d100d0f1c100e1d0e0f0f000d05000000000000000000000000";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ircode_test);

        mOperateList = new ArrayList<String>();
        mOperateList.add("1.获取产品类型");
        mOperateList.add("2.获取品牌类型");
        mOperateList.add("3.获取产品型号");
        mOperateList.add("4.根据123，获取临时下载URL");
        mOperateList.add("5.红外码识别，获取临时下载URL");
        mOperateList.add("6.下载红外码脚本文件");
        mOperateList.add("7.获取红外码基本信息");
        mOperateList.add("8.获取红外码数据");
        mOperateList.add("9.下载指定URL脚本文件");

        findView();
        setListener();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mOperateList);
        mIRCodeTestList.setAdapter(adapter);
    }

    private void findView() {
        mIRCodeTestList = (ListView) findViewById(R.id.ircode_test_list);
    }

    private void setListener(){
        mIRCodeTestList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        queryIRCodeType();
                        break;
                    case 1:
                        //获取空调产品所有品牌列表
                        //queryIRCodeBrand(3);

                        //查询电视所有品牌列表
                        queryIRCodeBrand(2);
                        break;
                    case 2:
                        //获取海尔空调所有型号列表
                        queryIRCodeVersion(3, 349);

                        //电视机无需再去查询型号列表

                        break;
                    case 3:
                        //获取海尔空调CAR-13PUN versionid = 1577 红码下载URL
                        queryIRCodeScriptDownloadUrl(3, 11, 1577);

                        //获取海尔电视红外下载URL列表
                        //queryIRCodeScriptDownloadUrl(1, 11, 0);
                        break;
                    case 4:
                        recognizeIRCode(ircodeHexString);
                        break;
                    case 5:{
                        if (mDownloadUrl != null) {
                            downloadIRCodeScript(mDownloadUrl, mSavePath);
                        }
                    }
                        break;
                    case 6:
                        queryIRCodeBaseInfo(mSavePath, null);
                        break;
                    case 7:
                        getIRCodeData();
                        break;
                    case 8:{
                        //根据家庭URL下载对应gz格式空调脚本  澳柯玛空调_210
                        //解密key为mkey字段，若不存在mkey字段，则无需解密 mScriptRandkey设置为null
                        String downloadUrl = "https://d0f94faa04c63d9b7b0b034dcf895656rccode.ibroadlink.com/publicircode/v2/app/getfuncfilebyfixedid?fixedid=32251048&mtag=gz&mkey=82d12f66";
                        mSavePath =  BLLet.Controller.queryIRCodePath() + File.separator + "82d12f66.gz";
                        mScriptRandkey = "82d12f66";

                        downloadIRCodeScript(downloadUrl, mSavePath);
                    }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void queryIRCodeType() {
        new QueryIRCodeTypeTask().execute();
    }

    private void queryIRCodeBrand(int type) {
        new QueryIRCodeBrandTask().execute(String.valueOf(type));
    }

    private void queryIRCodeVersion(int type, int brand) {
        new QueryIRCodeVersionTask().execute(String.valueOf(type), String.valueOf(brand));
    }

    private void queryIRCodeScriptDownloadUrl(int type, int brand, int version) {
        new QueryIRCodeScriptDownloadUrlTask().execute(String.valueOf(type), String.valueOf(brand), String.valueOf(version));
    }

    private void recognizeIRCode(String ircode) {
        new RecongnizeIRCodeTask().execute(ircode);
    }

    private void downloadIRCodeScript(String url, String savePath) {
        new DownLoadIRCodeScriptTask().execute(url, savePath);
    }

    private void queryIRCodeBaseInfo(String path, String key) {
        BLIRCodeInfoResult result = BLLet.Controller.queryIRCodeInfomation(path, key);
        Log.d(LOG_TAG, "error: " + String.valueOf(result.getStatus()));
        Log.d(LOG_TAG, "msg: " + result.getMsg());
        if (result.succeed()) {
            Log.d(LOG_TAG, "info :" + result.getInfomation());
        }
    }

    private void getIRCodeData() {
        BLQueryIRCodeParams params = new BLQueryIRCodeParams();
        params.setState(BLRMCloudAcConstants.IRDA_STATUS_OPEN);
        params.setMode(BLRMCloudAcConstants.IRDA_MODE_COOL);
        params.setTemperature(23);
        params.setDirect(0);
        params.setSpeed(0);
        params.setKey(BLRMCloudAcConstants.IRDA_KEY_TEMP_ADD);

        BLIRCodeDataResult result = BLLet.Controller.queryACIRCodeData(mSavePath, params, null);
        Log.d(LOG_TAG, "error: " + String.valueOf(result.getStatus()));
        Log.d(LOG_TAG, "msg: " + result.getMsg());
        if (result.succeed()) {
            Log.d(LOG_TAG, "iddata: " + result.getIrcode());
        }
    }

    class QueryIRCodeTypeTask extends AsyncTask<String, Void, BLBaseBodyResult> {
        @Override
        protected BLBaseBodyResult doInBackground(String... strings) {
            return BLLet.Controller.requestIRCodeDeviceTypes();
        }

        @Override
        protected void onPostExecute(BLBaseBodyResult blBaseBodyResult) {
            super.onPostExecute(blBaseBodyResult);
            Log.d(LOG_TAG, "error: " + String.valueOf(blBaseBodyResult.getStatus()));
            Log.d(LOG_TAG, "msg: " + blBaseBodyResult.getMsg());

            if (blBaseBodyResult.succeed()) {
                Log.d(LOG_TAG, "response: " + blBaseBodyResult.getResponseBody());
            }

        }
    }

    class QueryIRCodeBrandTask extends AsyncTask<String, Void, BLBaseBodyResult> {
        @Override
        protected BLBaseBodyResult doInBackground(String... strings) {
            int type = Integer.parseInt(strings[0]);
            return BLLet.Controller.requestIRCodeDeviceBrands(type);
        }

        @Override
        protected void onPostExecute(BLBaseBodyResult blBaseBodyResult) {
            super.onPostExecute(blBaseBodyResult);
            Log.d(LOG_TAG, "error: " + String.valueOf(blBaseBodyResult.getStatus()));
            Log.d(LOG_TAG, "msg: " + blBaseBodyResult.getMsg());

            if (blBaseBodyResult.succeed()) {
                Log.d(LOG_TAG, "response: " + blBaseBodyResult.getResponseBody());
            }
        }
    }

    class QueryIRCodeVersionTask extends AsyncTask<String, Void, BLBaseBodyResult> {
        @Override
        protected BLBaseBodyResult doInBackground(String... strings) {
            int type = Integer.parseInt(strings[0]);
            int brand = Integer.parseInt(strings[1]);
            return BLLet.Controller.requestIRCodeDeviceVersion(type, brand);
        }

        @Override
        protected void onPostExecute(BLBaseBodyResult blBaseBodyResult) {
            super.onPostExecute(blBaseBodyResult);
            Log.d(LOG_TAG, "error: " + String.valueOf(blBaseBodyResult.getStatus()));
            Log.d(LOG_TAG, "msg: " + blBaseBodyResult.getMsg());

            if (blBaseBodyResult.succeed()) {
                Log.d(LOG_TAG, "response: " + blBaseBodyResult.getResponseBody());
            }
        }
    }

    class QueryIRCodeScriptDownloadUrlTask extends AsyncTask<String, Void, BLBaseBodyResult> {
        @Override
        protected BLBaseBodyResult doInBackground(String... strings) {
            int type = Integer.parseInt(strings[0]);
            int brand = Integer.parseInt(strings[1]);
            int version = Integer.parseInt(strings[2]);
            return BLLet.Controller.requestIRCodeScriptDownloadUrl(type, brand, version);
        }

        @Override
        protected void onPostExecute(BLBaseBodyResult blBaseBodyResult) {
            super.onPostExecute(blBaseBodyResult);
            Log.d(LOG_TAG, "error: " + String.valueOf(blBaseBodyResult.getStatus()));
            Log.d(LOG_TAG, "msg: " + blBaseBodyResult.getMsg());

            if (blBaseBodyResult.succeed()) {
                Log.d(LOG_TAG, "response: " + blBaseBodyResult.getResponseBody());
                try {
                    JSONObject jResult = new JSONObject(blBaseBodyResult.getResponseBody());
                    JSONArray infos = jResult.optJSONArray("downloadinfo");

                    JSONObject info = infos.getJSONObject(0);
                    mDownloadUrl = info.optString("downloadurl", null);
                    mScriptRandkey = info.optString("randkey", null);
                    mScriptName = info.optString("name", null);
                    mSavePath = BLLet.Controller.queryIRCodePath() + File.separator + mScriptName;

                    Log.d(LOG_TAG, "mDownloadUrl:" + mDownloadUrl + " randkey:" + mScriptRandkey);

                } catch (Exception e) {

                }
            }
        }
    }

    class RecongnizeIRCodeTask extends AsyncTask<String, Void, BLBaseBodyResult> {
        @Override
        protected BLBaseBodyResult doInBackground(String... strings) {
            return BLLet.Controller.recognizeIRCode(strings[0]);
        }

        @Override
        protected void onPostExecute(BLBaseBodyResult blBaseBodyResult) {
            super.onPostExecute(blBaseBodyResult);
            Log.d(LOG_TAG, "error: " + String.valueOf(blBaseBodyResult.getStatus()));
            Log.d(LOG_TAG, "msg: " + blBaseBodyResult.getMsg());

            if (blBaseBodyResult.succeed()) {
                Log.d(LOG_TAG, "response: " + blBaseBodyResult.getResponseBody());
                try {
                    JSONObject jResult = new JSONObject(blBaseBodyResult.getResponseBody());
                    JSONArray infos = jResult.optJSONArray("downloadinfo");

                    JSONObject info = infos.getJSONObject(0);
                    mDownloadUrl = info.optString("downloadurl", null);
                    mScriptRandkey = info.optString("randkey", null);
                    mScriptName = info.optString("name", null);
                    mSavePath = BLLet.Controller.queryIRCodePath() + File.separator + mScriptName;

                    Log.d(LOG_TAG, "mDownloadUrl:" + mDownloadUrl + " randkey:" + mScriptRandkey);

                } catch (Exception e) {

                }
            }
        }
    }

    class DownLoadIRCodeScriptTask extends AsyncTask<String, Void, BLDownloadScriptResult> {
        @Override
        protected BLDownloadScriptResult doInBackground(String... strings) {
            return BLLet.Controller.downloadIRCodeScript(strings[0], strings[1], mScriptRandkey);
        }

        @Override
        protected void onPostExecute(BLDownloadScriptResult blDownloadScriptResult) {
            super.onPostExecute(blDownloadScriptResult);
            Log.d(LOG_TAG, "error: " + String.valueOf(blDownloadScriptResult.getStatus()));
            Log.d(LOG_TAG, "msg: " + blDownloadScriptResult.getMsg());

            if (blDownloadScriptResult.succeed()) {
                Log.d(LOG_TAG, "response: " + blDownloadScriptResult.getSavePath());
            }
        }
    }
}
