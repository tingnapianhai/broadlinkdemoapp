package cn.com.broadlink.blappsdkdemo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alibaba.fastjson.JSON;

import java.lang.reflect.Array;
import java.util.ArrayList;

import cn.com.broadlink.blappsdkdemo.R;
import cn.com.broadlink.blappsdkdemo.common.BLCommonUtils;
import cn.com.broadlink.blappsdkdemo.data.BLControlActConstans;
import cn.com.broadlink.sdk.BLLet;
import cn.com.broadlink.sdk.constants.controller.BLControllerErrCode;
import cn.com.broadlink.sdk.data.controller.BLDNADevice;
import cn.com.broadlink.sdk.data.controller.BLStdData;
import cn.com.broadlink.sdk.param.controller.BLStdControlParam;
import cn.com.broadlink.sdk.result.controller.BLProfileStringResult;
import cn.com.broadlink.sdk.result.controller.BLStdControlResult;

/**
 * Created by zhujunjie on 2016/11/11.
 */

public class DnaControlActivity extends Activity {
    private BLDNADevice mDNADevice;
    private EditText mValInputView;
    private EditText mParamInputView;
    private Button mDnaGetBtn;
    private Button mDnaSetBtn;
    private Button mDnaGetProfileBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dnacontrol_layout);

        mDNADevice = getIntent().getParcelableExtra("INTENT_DEV_ID");

        findView();
        setListener();
    }

    private void findView(){
        mValInputView = (EditText) findViewById(R.id.vals_input_view);
        mParamInputView = (EditText) findViewById(R.id.params_input_view);
        mDnaGetBtn = (Button) findViewById(R.id.dna_get_btn);
        mDnaSetBtn = (Button) findViewById(R.id.dna_set_btn);
        mDnaGetProfileBtn = (Button) findViewById(R.id.dna_get_profile_btn);
    }

    private void setListener(){
        mDnaGetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = mParamInputView.getText().toString();
                new DnaControlGetTask().execute(param);
            }
        });

        mDnaSetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String param = mParamInputView.getText().toString();
                String val = mValInputView.getText().toString();
                new DnaControlSetTask().execute(param, val);
            }
        });

        mDnaGetProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DnaControlGetProfileTask().execute();
            }
        });
    }

    class DnaControlGetTask extends AsyncTask<String, Void, BLStdControlResult> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DnaControlActivity.this);
            progressDialog.setMessage("Getting...");
            progressDialog.show();
        }

        @Override
        protected BLStdControlResult doInBackground(String... params) {
            String param = params[0];

            BLStdControlParam stdControlParam = new BLStdControlParam();
            stdControlParam.setAct(BLControlActConstans.ACT_GET);
            stdControlParam.getParams().add(param);

            return BLLet.Controller.dnaControl(mDNADevice.getDid(), null, stdControlParam);
        }

        @Override
        protected void onPostExecute(BLStdControlResult result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if(result != null && result.getStatus() == BLControllerErrCode.SUCCESS){
                BLStdData stdData = result.getData();
                BLCommonUtils.toastShow(DnaControlActivity.this, JSON.toJSONString(stdData));
            }
        }
    }

    class DnaControlSetTask extends AsyncTask<String, Void, BLStdControlResult> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DnaControlActivity.this);
            progressDialog.setMessage("Setting...");
            progressDialog.show();
        }

        @Override
        protected BLStdControlResult doInBackground(String... params) {
            String param = params[0];

            BLStdData.Value value = new BLStdData.Value();
            value.setVal(params[1]);

            ArrayList<BLStdData.Value> dnaVals = new ArrayList<>();
            dnaVals.add(value);

            BLStdControlParam stdControlParam = new BLStdControlParam();
            stdControlParam.setAct(BLControlActConstans.ACT_SET);
            stdControlParam.getParams().add(param);
            stdControlParam.getVals().add(dnaVals);

            return BLLet.Controller.dnaControl(mDNADevice.getDid(), null, stdControlParam);
        }

        @Override
        protected void onPostExecute(BLStdControlResult result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if(result != null && result.getStatus() == BLControllerErrCode.SUCCESS){
                BLStdData stdData = result.getData();
                BLCommonUtils.toastShow(DnaControlActivity.this, JSON.toJSONString(stdData));
            }
        }
    }

    class DnaControlGetProfileTask extends AsyncTask<String, Void, BLProfileStringResult> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DnaControlActivity.this);
            progressDialog.setMessage("Getting Profile...");
            progressDialog.show();
        }

        @Override
        protected BLProfileStringResult doInBackground(String... params) {
            return BLLet.Controller.queryProfile(mDNADevice.getDid());
        }

        @Override
        protected void onPostExecute(BLProfileStringResult result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if(result != null && result.getStatus() == BLControllerErrCode.SUCCESS){
                String profileStr = result.getProfile();
                BLCommonUtils.toastShow(DnaControlActivity.this, profileStr);
            }
        }
    }
}
