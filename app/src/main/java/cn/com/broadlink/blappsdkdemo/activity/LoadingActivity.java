package cn.com.broadlink.blappsdkdemo.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import java.io.File;

import cn.com.broadlink.blappsdkdemo.BLApplcation;
import cn.com.broadlink.blappsdkdemo.R;
import cn.com.broadlink.blappsdkdemo.common.BLFileUtils;
import cn.com.broadlink.sdk.BLLet;
import cn.com.broadlink.sdk.result.account.BLLoginResult;

public class LoadingActivity extends Activity{

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_layout);

        inits();
    }

    private void inits() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            initBLSdk();
            initAction();
        }
    }

    private void initAction() {
        copyAssertJSToAPP();
        toActivity();
    }

    private void toActivity(){
       if(BLApplcation.mBLUserInfoUnits.isLoggedIn()){
           Intent intent = new Intent();
           intent.setClass(LoadingActivity.this, MainActivity.class);
           startActivity(intent);

           //本地登录
           BLLoginResult loginResult = new BLLoginResult();
           loginResult.setUserid(BLApplcation.mBLUserInfoUnits.getUserid());
           loginResult.setLoginsession(BLApplcation.mBLUserInfoUnits.getLoginsession());
           loginResult.setIconpath(BLApplcation.mBLUserInfoUnits.getIconpath());
           loginResult.setNickname(BLApplcation.mBLUserInfoUnits.getNickname());
           loginResult.setSex(BLApplcation.mBLUserInfoUnits.getSex());
           loginResult.setLoginip(BLApplcation.mBLUserInfoUnits.getLoginip());
           loginResult.setLogintime(BLApplcation.mBLUserInfoUnits.getLogintime());
//           loginResult.setFlag(BLApplcation.mBLUserInfoUnits.getFlag());
           BLLet.Account.localLogin(loginResult);
       }else{
           Intent intent = new Intent();
           intent.setClass(LoadingActivity.this, LoginActivity.class);
           startActivity(intent);
       }
        finish();
    }

    //将assert目录下的文件拷贝到本地文件夹
    public void copyAssertJSToAPP() {
        String trgPath = BLLet.Controller.queryUIPath() + File.separator + "cordova.js";
        String srcPath = "js/cordova.js";
        BLFileUtils.copyAssertToSDCard(LoadingActivity.this, srcPath , trgPath);
    }

    private void initBLSdk(){
        BLLet.init(this);
        BLLet.DebugLog.on();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();

                    initBLSdk();
                    initAction();
                } else {
                    // Permission Denied
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
