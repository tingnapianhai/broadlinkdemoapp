package cn.com.broadlink.blappsdkdemo.activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import cn.com.broadlink.blappsdkdemo.R;
import cn.com.broadlink.sdk.BLLet;
import cn.com.broadlink.sdk.param.family.BLFamilyAllInfo;
import cn.com.broadlink.sdk.result.family.BLAllFamilyInfoResult;

public class FamilyDetailActivity extends Activity {

    private String mFamilyId;
    private BLFamilyAllInfo blFamilyAllInfo;

    private TextView mFamilyIdText;
    private TextView mFamilyNameText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_detail);

        findView();

        mFamilyId = getIntent().getStringExtra("INTENT_FAMILY_ID");
    }

    @Override
    protected void onResume() {
        super.onResume();

        getFamilyAllInfoFromCloud();
    }

    private void findView() {
        mFamilyIdText = (TextView) findViewById(R.id.family_id);
        mFamilyNameText = (TextView) findViewById(R.id.family_name);
    }

    private void getFamilyAllInfoFromCloud() {
        new FamilyAllInfoTask().execute(mFamilyId);
    }

    class FamilyAllInfoTask extends AsyncTask<String, Void, BLAllFamilyInfoResult> {

        @Override
        protected BLAllFamilyInfoResult doInBackground(String... strings) {
            String familyId = strings[0];

            String[] ids = {familyId};
            return BLLet.Family.queryAllFamilyInfos(ids);
        }

        @Override
        protected void onPostExecute(BLAllFamilyInfoResult blAllFamilyInfoResult) {
            super.onPostExecute(blAllFamilyInfoResult);
            if (blAllFamilyInfoResult.succeed()) {
                blFamilyAllInfo = blAllFamilyInfoResult.getAllInfos().get(0);
                mFamilyIdText.setText(blFamilyAllInfo.getFamilyInfo().getFamilyId());
                mFamilyNameText.setText(blFamilyAllInfo.getFamilyInfo().getFamilyName());
            }
        }
    }
}
